import React from 'react';
import { Table } from 'antd';
import { CheckOutlined, StopOutlined } from "@ant-design/icons";

const MergedTable = React.memo( (props) => {
    const sameKey = new Set();
    let countChildrenCompetence=[];
    let currentCompetenceIndex=0;
    let currentIndex=0;
    React.useEffect(() => {
        sameKey.clear();
        countChildrenCompetence=[];
        currentCompetenceIndex=0;
        currentIndex=0;
    });
    const columns = [
        {
            title: 'Năng lực',
            dataIndex: 'competence',
            key: 'competence',
            fontWeight: 'bold',
            rowSpan: 1, 
            width: "30%",
            render: (competence,dataRow, row, index) => {
              const obj = {
                children: <div style={{fontWeight:700}}> {competence.competenceName} </div>,
                props: {}
              };
                if (sameKey.has(competence.competenceName)) {
                obj.props.rowSpan = 0;
                obj.props.className="a";
              } else {
                const occurCount =  props.dataSource.filter(data => data.competence.competenceName === competence.competenceName).length;
                obj.props.rowSpan = occurCount;
                sameKey.add(competence.competenceName);
                countChildrenCompetence.push({id:  competence.id, length:  occurCount});
              }
              return obj;
            }
        }, 
        { 
          title: "Câu hỏi", dataIndex: "question",
          render: (question, dataRow, ) => {
            if (props.dataSource.length===1) return ( <div> 1. {question.name}</div>)
            if (countChildrenCompetence.length>0)
            {
              if (countChildrenCompetence[currentCompetenceIndex]&&countChildrenCompetence[currentCompetenceIndex].id === dataRow.competence.id)
              {
                currentIndex++;
              }
              else {
                currentCompetenceIndex+=1
                currentIndex=1;
              }
              return (            
                <div> {currentIndex}. {question.name}</div>
              )
            }
          },
        },        
        { 
          title: "Kết quả", dataIndex: "isRight", align: "center",
          render: (isRight) => (
            isRight===1?<CheckOutlined style={{color:"green"}} />:<StopOutlined style={{color:"red"}}/>
          ),
        },
      ];
    return(
    <Table
        columns={columns}
        dataSource={props.dataSource}
        className="base-table mergedtable"
        size="middle"
        scroll={{ x: true }}
        {...props}
    />)
})

export default MergedTable;