import React from "react";
import { Form, Col, Row, Input } from "antd";

import BasicForm from "../common/entryForm/BasicForm";
import TextField from "../common/entryForm/TextField";
import { ProvinceKinds } from "../../constants";
import { commonStatus } from "../../constants/masterData";
import DropdownField from "../common/entryForm/DropdownField";

class ProvinceForm extends BasicForm {

  getInitialFormValues = () => {
    const { isEditing, dataDetail } = this.props;
    if(!isEditing){
      return {
        ...dataDetail,
        status: 1
      }
    }
    return dataDetail;
  }

  findProvinceKindByKey = (key, value) => {
    return ProvinceKinds[Object.keys(ProvinceKinds).find(e=>ProvinceKinds[e][key]===value)];
  }

  render() {
    const { formId, dataDetail, parentProvinces, loadingSave } = this.props;
    return (
      <Form
        id={formId}
        ref={this.formRef}
        layout="vertical"
        onFinish={this.handleSubmit}
        initialValues={this.getInitialFormValues()}
      >
        {
          parentProvinces && parentProvinces.map((e, i)=>{
            if(e.id || e.id === 0){
              return (
                <Row gutter={16} key={e.id}>
                  <Col span={16}>
                    <Form.Item
                      label={`Tên ${ProvinceKinds[Object.keys(ProvinceKinds)[i]].text.toLowerCase()}`}
                    >
                      <Input disabled value={e.name}/>
                    </Form.Item>
                  </Col>
                </Row>
              )
            }
            return null;
          })
        }
        <Row gutter={16}>
          <Col span={16}>
            <TextField
              fieldName="provinceName"
              min={6}
              label={`Tên ${this.findProvinceKindByKey('name', dataDetail.kind ).text.toLowerCase()}`}
              required
              placeholder={`Vui lòng nhập tên ${this.findProvinceKindByKey('name', dataDetail.kind ).text.toLowerCase()}`}
              disabled={loadingSave}
            />
          </Col>
        </Row>

        <Row gutter={16}>
            <Col span={16}>
              <DropdownField
                fieldName="status"
                label="Trạng thái"
                required
                options={commonStatus}
                disabled={loadingSave}
              />
            </Col>
        </Row>
        <Row gutter={16}>
          <Col span={16} hidden>
            <TextField fieldName="parentId" label="Parent ID" disabled/>
            <TextField fieldName="kind" label="Loại" disabled/>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default ProvinceForm;
