import React, {Component} from 'react';

import { commonQuestionKind } from "../../constants/masterData";

class BaseQuestion extends Component {

    handleComponent = () => {
        const { kind } = this.props;
        let QComponent = commonQuestionKind.find(c=>{
            if(c.value === kind)return true;
            return false;
        });
        QComponent = QComponent ? QComponent.Component : <div></div>;
        return (<div className="question-component">
                <QComponent {...this.props}/>
        </div>)
    }
    render() {
        return (
            this.handleComponent()
        );
    }
}

export default BaseQuestion;
