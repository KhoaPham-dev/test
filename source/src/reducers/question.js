import { actionTypes, reduxUtil } from '../actions/question';

const { createReducer, defineActionSuccess, defineActionLoading, defineActionFailed } = reduxUtil;
const {
    GET_QUESTION_LIST,
    DELETE_QUESTION,
    GET_COMBOBOX_COMPETENCES,
} = actionTypes;

const initialState = { 
    questionData: [],
    tbQuestionLoading: false,
    comboboxCompetencesData: []
};

const reducer = createReducer({
    [defineActionLoading(GET_QUESTION_LIST)]: (state) => {
        return {
            ...state,
            tbQuestionLoading: true
        }
    },
    [defineActionSuccess(GET_QUESTION_LIST)]: (state, { questionData }) => {
        return {
            ...state,
            questionData,
            tbQuestionLoading: false
        }
    },
    [defineActionLoading(DELETE_QUESTION)] : (state) =>{
        return {
            ...state,
            tbQuestionLoading: true,
        }
    },
    [defineActionFailed(DELETE_QUESTION)] : (state) =>{
        return {
            ...state,
            tbQuestionLoading: false,
        }
    },
    [defineActionSuccess(GET_COMBOBOX_COMPETENCES)] : (state, { comboboxCompetencesData }) =>{
        return {
            ...state,
            comboboxCompetencesData,
        }
    },
    initialState
})

export default {
    reducer
};
