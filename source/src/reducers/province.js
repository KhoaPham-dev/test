import { actionTypes, reduxUtil } from '../actions/province';

const { createReducer, defineActionSuccess, defineActionLoading, defineActionFailed } = reduxUtil;
const {
    GET_PROVINCE_LIST,
    GET_PROVINCE_COMBOBOX,
    DELETE_PROVINCE,
} = actionTypes;

const initialState = { 
    provinceData: {},
    provinceCombobox: [],
    districtCombobox: [],
    wardCombobox: [],
    tbProvinceLoading: false,
};

const reducer = createReducer({
    [defineActionLoading(GET_PROVINCE_LIST)]: (state) => {
        return {
            ...state,
            tbProvinceLoading: true
        }
    },
    [defineActionSuccess(GET_PROVINCE_LIST)]: (state, { provinceData }) => {
        return {
            ...state,
            provinceData,
            tbProvinceLoading: false
        }
    },
    [defineActionSuccess(GET_PROVINCE_COMBOBOX)]: (state, { comboboxData, comboboxType }) => {
        state[comboboxType] = comboboxData;
        return {
            ...state,
        }
    },
    [defineActionLoading(DELETE_PROVINCE)] : (state) =>{
        return {
            ...state,
            tbProvinceLoading: true,
        }
    },
    [defineActionFailed(DELETE_PROVINCE)] : (state) =>{
        return {
            ...state,
            tbProvinceLoading: false,
        }
    },
    initialState
})

export default {
    reducer
};
