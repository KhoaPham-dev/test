import {actionTypes, reduxUtil} from "../actions/category";


const { createReducer, defineActionSuccess, defineActionLoading, defineActionFailed } = reduxUtil;

const {
    GET_CATEGORY_LIST,
    GET_UNIVERSITY_CATEGORY_LIST,
    GET_JOB_CATEGORY_LIST,
    DELETE_CATEGORY,
} = actionTypes;

const initialState = {
    categoryListData: {},
    parent: [],
    categoryListLoading: false,
}

const reducer = createReducer ({
    [defineActionLoading(GET_CATEGORY_LIST,)] : (state) =>{
        return {
            ...state,
            categoryListData: {},
            categoryListLoading: true,
        }
    },
    [defineActionSuccess(GET_CATEGORY_LIST,)] : (state, {categoryListData, parent} ) =>{
        return {
            ...state,
            categoryListData,
            parent,
            categoryListLoading: false,
        }
    },
    [defineActionLoading(GET_UNIVERSITY_CATEGORY_LIST,)] : (state) =>{
        return {
            ...state,
            categoryListData: {},
            categoryListLoading: true,
        }
    },
    [defineActionSuccess(GET_UNIVERSITY_CATEGORY_LIST,)] : (state, {categoryListData, parent} ) =>{
        return {
            ...state,
            categoryListData,
            parent,
            categoryListLoading: false,
        }
    },
    [defineActionLoading(GET_JOB_CATEGORY_LIST,)] : (state) =>{
        return {
            ...state,
            categoryListData: {},
            categoryListLoading: true,
        }
    },
    [defineActionSuccess(GET_JOB_CATEGORY_LIST,)] : (state, {categoryListData, parent} ) =>{
        return {
            ...state,
            categoryListData,
            parent,
            categoryListLoading: false,
        }
    },
    [defineActionLoading(DELETE_CATEGORY,)] : (state) =>{
        return {
            ...state,
            categoryListLoading: true,
        }
    },
    [defineActionFailed(DELETE_CATEGORY,)] : (state) =>{
        return {
            ...state,
            categoryListLoading: false,
        }
    },
    initialState
})

export default {
    reducer
};