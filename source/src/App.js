import React from 'react';
import { Provider } from 'react-redux';

import RootRoute from './routes/index';
import configureStore from './store';
import firebase from 'firebase'
const store = configureStore();

const App = () => {
    return (
        <Provider store={store}>
           <RootRoute/>
        </Provider>
    )
}

export default App;
