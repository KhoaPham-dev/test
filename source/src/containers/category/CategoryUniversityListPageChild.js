import React from 'react';
import { connect } from 'react-redux';
import { Button } from 'antd';
import { PlusOutlined,} from '@ant-design/icons';

import ListBasePage from '../ListBasePage';
import CategoryForm from '../../compoments/category/CategoryForm';
import BaseTable from '../../compoments/common/table/BaseTable';
import BasicModal from '../../compoments/common/modal/BasicModal';

import { actions } from '../../actions';

import qs from 'query-string';
import { sitePathConfig } from '../../constants/sitePathConfig';
import { CategoryKinds } from "../../constants";
import { showSucsessMessage } from '../../services/notifyService';

class CategoryUniversityListPageChild extends ListBasePage {
    initialSearch() {
        return { name: "",};
    }

    constructor(props) {
        super(props);
        this.objectName = 'danh mục khoa';

        this.breadcrumbs = [{name: 'Danh mục khoa'}];
        this.columns = [
            this.renderIdColumn(),
            {title: 'Tên', dataIndex: 'categoryName'},
            // { title: 'Mô tả', dataIndex: 'description'},
            this.renderActionColumn()
        ];
        this.actionColumns = {
            isEdit: true,
            isDelete: true,
        }
    }

    getList() {
        const { getDataList } = this.props;
        const temp = parseInt(decodeURI(this.props.location.pathname.split("&parentId=")[1]));
        const page = this.pagination.current ? this.pagination.current - 1 : 0;
        const params = {parentId: temp, page, size: this.pagination.pageSize, search: this.search};
        getDataList({ params });
    }
    getSearchFields() {
        return [
            { key: "name", seachPlaceholder: 'Tên danh mục khoa', 
                initialValue: this.search.parentname? this.search.parentname : this.search.name
            },
        ];
    }

    prepareCreateData(values) {
        return {
            ...values,
            parentId:parseInt(decodeURI(this.props.location.pathname.split("&parentId=")[1])),
        };
    }
    
    prepareUpdateData(data) {
        return {
            ...data,
            categoryId: this.dataDetail.id,
        };
    }

    getDataDetailMapping(data) {
        return {
            ...data,
        }
    }

 
    renderRouting (){
        const childsearch=this.renderSearchBeforeRouting();     
        this.props.history.push(`${sitePathConfig.categoryuniversity.path}?${qs.stringify(childsearch)}`);
    }
    renderSearchBeforeRouting()
    {
        const currentsearch=this.setQueryString();
        Object.keys(currentsearch)
        .forEach(key => {
            if(!([key].toString().includes("parent")))
                delete currentsearch[key];
        });
        return currentsearch;
    }
    setQueryString() {
        const { location: { pathname, search }, history } = this.props;
        const queryString = qs.parse(search);
        let newQsValue = {};
        if(this.pagination.current > 1) {
            newQsValue.page = this.pagination.current;
        }
        else  {
            delete queryString.page;
        }
        newQsValue = Object.assign(queryString, newQsValue, this.search);
        const refactorParentProvinces = {};
            Object.keys(this.search).forEach(key => refactorParentProvinces[key]=this.search[key]);
            newQsValue=Object.assign(newQsValue,refactorParentProvinces);
        if(Object.keys(newQsValue).length > 0)
        {
            Object.keys(newQsValue).forEach(key => {
                if(!newQsValue[key])
                    delete newQsValue[key];
             });
        }

        if(Object.keys(newQsValue).length > 0)
            history.push(`${pathname}?${qs.stringify(newQsValue)}`);            
        else
            history.push(pathname);
        return(newQsValue);
    }
    
    sliceparentLocationSearch(locationsearch)
    {
        var searchParams = new URLSearchParams(locationsearch);
        let temp = {}
        temp.name = searchParams.get("parentname");
        return temp;
    }
    loadDataTable(currentProps) {
        const queryString = qs.parse(currentProps.location.search);
        this.pagination.current = 1;
        if(!isNaN(queryString.page))
            this.pagination.current = parseInt(queryString.page);
        Object.keys(this.search).forEach(key => this.search[key] = queryString[key]);
        this.getList();
        this.props.history.replace(`${this.props.location.pathname}?${qs.stringify(queryString)}`);          
    }
    onDeleteCompleted() {
        const { dataList } = this.props;
        if(dataList && this.pagination.current > 1 && dataList.data && dataList.data.length === 1) {
            this.pagination.current = this.pagination.current - 1;
            this.setQueryString();
        }
        else {
            this.getList();
        }
        showSucsessMessage(`Xóa ${this.objectName} thành công!`);
    }
    render() {
        const { dataList, loading } = this.props;
        const { isShowModifiedModal, isShowModifiedLoading } = this.state;
        this.pagination.total = dataList.totalElements || 0;
        this.pagination.pageSize = 30;

        let category = dataList.data;
        return (
            <div>
                {this.renderSearchForm()}
                <div className="action-bar category">   
                    <span style={{color: "#40a9ff"}}>

                    {<button style={{margin: "0 5px", borderWidth: 0, background: "none"}} onClick={()=>this.renderRouting()}>Tất cả</button>}
                    {<span style={{margin: "0 5px", color: 'black'}}>></span>}
                    {decodeURI(this.props.location.pathname.split("&parentId=")[0].split("parentName=")[1])}
                    </span> 
                    
                    <Button type="primary" onClick={() => this.onShowModifiedModal(false)}>
                    <PlusOutlined /> Thêm khoa
                    </Button>
                </div>

                <BaseTable
                    loading={loading}
                    columns={this.columns}
                    rowKey={record => record.id}
                    dataSource={category}
                    pagination={this.pagination}
                    onChange={this.handleTableChange}
                />
                <BasicModal
                    visible={isShowModifiedModal}
                    isEditing={this.isEditing}
                    objectName={this.objectName}
                    loading={isShowModifiedLoading}
                    onOk={this.onOkModal}
                    onCancel={this.onCancelModal}
                    >
                        <CategoryForm
                            isEditing={this.isEditing}
                            dataDetail={this.isEditing ? {...this.dataDetail} : {}}
                            parentId={parseInt(decodeURI(this.props.location.pathname.split("&parentId=")[1]))}    
                            loadingSave={isShowModifiedLoading}         
                        />
                </BasicModal>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    loading: state.category.categoryListLoading,
    dataList: state.category.categoryListData || {},
})

const mapDispatchToProps = dispatch => ({
    getDataList: (payload) => dispatch(actions.getUniversityCategoryList(payload)),
    getDataById: (payload) => dispatch(actions.getCategory(payload)),
    createData: (payload) => dispatch(actions.createCategory(payload)),
    updateData: (payload) => dispatch(actions.updateCategory(payload)),
    deleteData: (payload) => dispatch(actions.deleteCategory(payload)),
})

export default connect(mapStateToProps, mapDispatchToProps)(CategoryUniversityListPageChild);