import React from 'react';
import { connect } from 'react-redux';
import { Button, } from 'antd';
import { PlusOutlined,} from '@ant-design/icons';

import ListBasePage from '../ListBasePage';
import CategoryForm from '../../compoments/category/CategoryForm';
import BaseTable from '../../compoments/common/table/BaseTable';
import BasicModal from '../../compoments/common/modal/BasicModal';

import { actions } from '../../actions';
import qs from 'query-string';
import { sitePathConfig } from '../../constants/sitePathConfig';
import { CategoryKinds } from "../../constants";
import { showSucsessMessage } from '../../services/notifyService';

class CategoryListPage extends ListBasePage {
    initialSearch() {
        return { name: "",};
    }

    constructor(props) {
        super(props);
        this.objectName = 'thể loại';
        this.breadcrumbs = [{name: 'Thể loại'}];
        this.columns = [
            this.renderIdColumn(),
            {
                title: 'Tên', 
                render: (dataRow) => {
                    return (
                        <span style={{cursor: 'pointer', color: "#1890ff"}} onClick={()=>{
                            this.renderRouting(dataRow.id,dataRow.categoryName);
                        }}>
                            {dataRow.categoryName}
                        </span>
                    )
                }
            },
            // { title: 'Mô tả', dataIndex: 'description'},
            this.renderActionColumn()
        ];
        this.actionColumns = {
            isEdit: true,
            isDelete: true,
            isChangeStatus: false
        }
    }

    getSearchFields() {
        let searchparentname = this.props.location && this.props.location.search? this.sliceLocationSearch(this.props.location.search).name : this.search.name
        this.search.name = searchparentname;
        return [
            { 
                key: "name", 
                seachPlaceholder: 'Tên thể loại', 
                initialValue: searchparentname,
            },
        ];
    }

    prepareCreateData(values) {
        return {
            kind: CategoryKinds.CATEGORY_KIND_NEWS,
            ...values,
        };
    }
    
    prepareUpdateData(data) {
        return {
            ...data,
            categoryId: this.dataDetail.id,
        };
    }


    getDataDetailMapping(data) {
        return {
            ...data,
            parentId: this.dataDetail.id,
        }
    }

    renderRouting (id,name){
        const parentsearch=this.renderSearchBeforeRouting();     
        this.props.history.push(`${sitePathConfig.category.path}/parentName=${name}&parentId=${id}?${qs.stringify(parentsearch)}`);
    }

    renderSearchBeforeRouting()
    {
        const currentsearch=this.setQueryString();
        Object.keys(currentsearch)
        .forEach(key => {
            if(!([key].toString().includes("parent")))
                delete currentsearch[key];
        });
        return currentsearch;
    }

    sliceLocationSearch(locationsearch)
    {
        var searchParams = new URLSearchParams(locationsearch);
        let temp = {}
        temp.name = searchParams.get("parentname");
        return temp;
    }
    onSearch(values) {
        this.search=Object.assign(this.search, values);
        this.pagination.current = 1;
        this.setQueryString();
    }
    loadDataTable(currentProps) {
        const queryString = qs.parse(currentProps.location.search);
        if (queryString.parentname)
            queryString.name = queryString.parentname;
        this.pagination.current = 1;
        if(!isNaN(queryString.page))
            this.pagination.current = parseInt(queryString.page);
        Object.keys(this.search).forEach(key => this.search[key] = queryString[key]);
        this.getList();
        this.props.history.replace(`${this.props.location.pathname}?${qs.stringify(queryString)}`);          
    }
    setQueryString() {
        const { location: { pathname, search }, history } = this.props;
        const queryString = qs.parse(search);
        let newQsValue = {};
        if(this.pagination.current > 1) {
            newQsValue.page = this.pagination.current;
        }
        else  {
            delete queryString.page;
        }
        newQsValue = Object.assign(queryString, newQsValue, this.search);
        const refactorParentProvinces = {};
            Object.keys(this.search).forEach(key => refactorParentProvinces[key]=this.search[key]);
            Object.keys(refactorParentProvinces).forEach(key=> refactorParentProvinces["parent"+key]=refactorParentProvinces[key]);
            newQsValue=Object.assign(newQsValue,refactorParentProvinces);
        if(Object.keys(newQsValue).length > 0)
        {
            Object.keys(newQsValue).forEach(key => {
                if(!newQsValue[key])
                    delete newQsValue[key];
             });
        }
        if(Object.keys(newQsValue).length > 0)
            history.push(`${pathname}?${qs.stringify(newQsValue)}`);            
        else
            history.push(pathname);
        return(newQsValue);
    }
    onDeleteCompleted() {
        const { dataList } = this.props;
        if(dataList && this.pagination.current > 1 && dataList.data && dataList.data.length === 1) {
            this.pagination.current = this.pagination.current - 1;
            this.setQueryString();
        }
        else {
            this.getList();
        }
        showSucsessMessage(`Xóa ${this.objectName} thành công!`);
    }
    render() {
        const { dataList, loading, } = this.props;
        const { isShowModifiedModal, isShowModifiedLoading } = this.state;
        this.pagination.total = dataList.totalElements || 0;
        this.pagination.pageSize = 30;
        let category = [];
        let j = 0;
        if (dataList.data)
            for (let i = 0; i <this.pagination.total;i++)
                if (dataList.data[i] && !dataList.data[i].parentId)
                    category[j++] = dataList.data[i];
        return (
            <div>
                {this.renderSearchForm()}
                <div className="action-bar">    
                    {this.renderCreateNewButton((
                        <Button
                        type="primary"
                        onClick={() => this.onShowModifiedModal(false)}
                    >
                        <PlusOutlined /> Thêm thể loại
                    </Button>
                    ))}
                </div>
                <BaseTable
                    loading={loading}
                    columns={this.columns}
                    rowKey={record => record.id}
                    dataSource={category}
                    pagination={this.pagination}
                    onChange={this.handleTableChange}
                />
                <BasicModal
                    visible={isShowModifiedModal}
                    isEditing={this.isEditing}
                    objectName={this.objectName}
                    loading={isShowModifiedLoading}
                    onOk={this.onOkModal}
                    onCancel={this.onCancelModal}
                    >
                        <CategoryForm
                            isEditing={this.isEditing}
                            dataDetail={this.isEditing ?{...this.dataDetail} : {}}
                            loadingSave={isShowModifiedLoading}
                        />
                </BasicModal>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    loading: state.category.categoryListLoading,
    dataList: state.category.categoryListData || {},
    parent: state.category.parent,
})

const mapDispatchToProps = dispatch => ({
    getDataList: (payload) => dispatch(actions.getCategoryList(payload)),
    getDataById: (payload) => dispatch(actions.getCategory(payload)),
    createData: (payload) => dispatch(actions.createCategory(payload)),
    updateData: (payload) => dispatch(actions.updateCategory(payload)),
    deleteData: (payload) => dispatch(actions.deleteCategory(payload)),

})

export default connect(mapStateToProps, mapDispatchToProps)(CategoryListPage);