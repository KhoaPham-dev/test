import React from "react";
import { connect } from "react-redux";
import { Button, Avatar } from "antd";
import { PlusOutlined, FileTextOutlined } from "@ant-design/icons";

import ListBasePage from "../ListBasePage";
import NewsForm from "../../compoments/news/NewsForm";
import BaseTable from "../../compoments/common/table/BaseTable";
import BasicModal from "../../compoments/common/modal/BasicModal";
import { FieldTypes } from "../../constants/formConfig";
import { commonStatus } from "../../constants/masterData";
import { convertUtcToLocalTime } from "../../utils/datetimeHelper";

import { actions } from "../../actions";
import {
  AppConstants,
} from "../../constants";

class NewsListPage extends ListBasePage {
  initialSearch() {
    return { newTitle: "", status: undefined, categoryId: undefined};
  }

  constructor(props) {
    super(props);
    this.objectName = "Tin tức";
    this.breadcrumbs = [{ name: "Tin tức" }];
    this.columns = [
      this.renderIdColumn(),
      {
        title: "Ảnh đại diện",
        dataIndex: "newAvatar",
        align: "center",
        width: 110,
        render: (avatar) => (
          <Avatar
            style={{width: "100px", height: "100px", borderRadius: "0", padding: "8px"}}
            size="large"
            icon={<FileTextOutlined />}
            src={avatar ? `${AppConstants.contentRootUrl}${avatar}` : null}
          />
        ),
      },
      { title: "Tiêu đề", dataIndex: "newTitle" },
      { 
        title: <div style={{paddingRight: "20px"}}>Ngày tạo</div>, 
        align: "right",
        width: 135, 
        dataIndex: "createdDate",
        render: (createdDate) => (
          <div style={{paddingRight: "20px"}}>
            {convertUtcToLocalTime(createdDate, "DD/MM/YYYY")}
          </div>
        )
      },
      { 
        title: <div style={{paddingRight: "16px"}}>Thứ tự</div>, 
        dataIndex: "newOrdering",
        align: "right",
        width: 100,
        render: (newOrdering) => (
          <div style={{paddingRight: "16px"}}>{newOrdering}</div>
        )
      },
      {
        title: "Thể loại",
        dataIndex: "categoryId",
        width: 100,
        render: (categoryId) => {
          let category;
          if(this.categoryOptions.length > 0){
            category =  this.categoryOptions.find(e=>e.value===categoryId);
          }
          return (
          <span>
            {category ? category.label : ''}
          </span>
        )},
      },
      this.renderStatusColumn(),
      this.renderActionColumn(),
    ];
    this.actionColumns = {
      isEdit: true,
      isDelete: true,
      isChangeStatus: false,
    };
    this.props.getComboboxNewsList();
    this.categoryOptions = [];
  }

  getSearchFields() {
    const {comboboxNewsListLoading} = this.props;
    return [
      {
        key: "newTitle",
        seachPlaceholder: "Chọn Tiêu đề",
        initialValue: this.search.newTitle,
      },
      {
        key: "status",
        seachPlaceholder: "Chọn trạng thái",
        fieldType: FieldTypes.SELECT,
        options: commonStatus,
        initialValue: this.search.status,
      },
      {
        key: "categoryId",
        seachPlaceholder: "Chọn Thể loại",
        fieldType: FieldTypes.SELECT,
        options: [...this.categoryOptions],
        initialValue: this.search.categoryId,
        loading: comboboxNewsListLoading
      },
    ];
  }

  prepareCreateData(data) {
    return {
      ...data,
      newKind: this.props.comboboxNewsListData.data[0].kind,
    }
  }

    prepareUpdateData(data) {
      const { comboboxNewsListData } = this.props;
      return {
        ...data,
        newKind: comboboxNewsListData.data && comboboxNewsListData.data[0].kind,
        id: this.dataDetail.id
      }
    }


  render() {
    const {
      dataList,
      loading,
      comboboxNewsListData,
      uploadFile,
    } = this.props;
    const { isShowModifiedModal, isShowModifiedLoading } = this.state;
    const news = dataList.data || [];
    this.pagination.total = dataList.totalElements || 0;
    this.categoryOptions = comboboxNewsListData.data ? comboboxNewsListData.data.map(c=>{
      return {
        value: c.categoryId,
        label: c.categoryName,
      }
    }) : [];
    return (
      <div>
        {this.renderSearchForm()}
        <div className="action-bar">
          {
            this.renderCreateNewButton((
              <Button
                type="primary"
                onClick={() => this.onShowModifiedModal(false)}
              >
                <PlusOutlined /> Tin tức mới
              </Button>
            ))
          }
        </div>
        <BaseTable
          loading={loading}
          columns={this.columns}
          rowKey={(record) => record.id}
          dataSource={news}
          pagination={this.pagination}
          onChange={this.handleTableChange}
        />
        <BasicModal
          visible={isShowModifiedModal}
          isEditing={this.isEditing}
          objectName={this.objectName}
          loading={isShowModifiedLoading}
          onOk={this.onOkModal}
          onCancel={this.onCancelModal}
        >
          <NewsForm
            isEditing={this.isEditing}
            dataDetail={this.isEditing ? this.dataDetail : {}}
            categoryOptions={this.categoryOptions}
            uploadFile={uploadFile}
            loadingSave={isShowModifiedLoading}
          />
        </BasicModal>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  loading: state.news.newsListLoading,
  dataList: state.news.newsListData || {},
  comboboxNewsListLoading: state.news.comboboxNewsListLoading,
  comboboxNewsListData: state.news.comboboxNewsListData || {},
});

const mapDispatchToProps = (dispatch) => ({
  getDataList: (payload) => dispatch(actions.getNewsList(payload)),
  getDataById: (payload) => dispatch(actions.getNewsById(payload)),
  createData: (payload) => dispatch(actions.createNews(payload)),
  updateData: (payload) => dispatch(actions.updateNews(payload)),
  deleteData: (payload) => dispatch(actions.deleteNews(payload)),
  getComboboxNewsList: (payload) => dispatch(actions.getComboboxNewsList(payload)),
  uploadFile: (payload) => dispatch(actions.uploadFile(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsListPage);
