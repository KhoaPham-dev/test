import { call, put, takeLatest } from "redux-saga/effects";
import {actionTypes, reduxUtil } from "../actions/category";

import apiConfig from "../constants/apiConfig";

import { sendRequest } from "../services/apiService";
import { handleApiResponse } from "../utils/apiHelper";
import {CategoryKinds} from '../constants';
const {defineActionLoading, defineActionSuccess, defineActionFailed} = reduxUtil;

const {
    GET_CATEGORY_LIST,
    GET_UNIVERSITY_CATEGORY_LIST,
    GET_JOB_CATEGORY_LIST,
    DELETE_CATEGORY,
} = actionTypes;

function* getCategoryList({ payload: {params} }){
    const apiParams = apiConfig.category.getCategoryList;
    const searchParams = { page: params.page, size: 30,};
    if(params)
    {
        if (params.parentId) {
            searchParams.parentId = params.parentId
        }
        else 
        {
            searchParams.kind = CategoryKinds.CATEGORY_KIND_NEWS
        }
    }

    if (params.search)
    {
        if (params.search.name)
            searchParams.name = params.search.name;
    }

    try {
        const result = yield call (sendRequest, apiParams, searchParams);
        yield put ({
            type:defineActionSuccess(GET_CATEGORY_LIST),
            categoryListData: result.responseData && {
                ...result.responseData.data,
                parentId: params.parentId !== undefined ? params.parentId : -1
            },
        })
    }
    catch(error) {
        yield put({ type: defineActionFailed(GET_CATEGORY_LIST) });
    }
}
function* getUniversityCategoryList({ payload: {params} }){
    const apiParams = apiConfig.category.getCategoryList;
    const searchParams = { page: params.page, size: 30,};
    if(params)
    {
        if (params.parentId) {
            searchParams.parentId = params.parentId
        }
        else 
        {
            searchParams.kind = CategoryKinds.CATEGORY_KIND_UNIVERSITY
        }
    }

    if (params.search)
    {
        if (params.search.name)
            searchParams.name = params.search.name;
    }

    try {
        const result = yield call (sendRequest, apiParams, searchParams);
        yield put ({
            type:defineActionSuccess(GET_CATEGORY_LIST),
            categoryListData: result.responseData && {
                ...result.responseData.data,
                parentId: params.parentId !== undefined ? params.parentId : -1
            },
        })
    }
    catch(error) {
        yield put({ type: defineActionFailed(GET_CATEGORY_LIST) });
    }
}

function* getJobCategoryList({ payload: {params} }){
    const apiParams = apiConfig.category.getCategoryList;
    const searchParams = { page: params.page, size: 30,};
    if(params)
    {
        if (params.parentId) {
            searchParams.parentId = params.parentId
        }
        else 
        {
            searchParams.kind = CategoryKinds.CATEGORY_KIND_JOB
        }
    }

    if (params.search)
    {
        if (params.search.name)
            searchParams.name = params.search.name;
    }

    try {
        const result = yield call (sendRequest, apiParams, searchParams);
        yield put ({
            type:defineActionSuccess(GET_CATEGORY_LIST),
            categoryListData: result.responseData && {
                ...result.responseData.data,
                parentId: params.parentId !== undefined ? params.parentId : -1
            },
        })
    }
    catch(error) {
        yield put({ type: defineActionFailed(GET_CATEGORY_LIST) });
    }
}

function* getCategory ({payload:{params, onCompleted, onError}})
{
    try {
        //Define which Api and its path
        const apiParams = {
            ...apiConfig.category.getById,
            path: `${apiConfig.category.getById.path}/${params.id}`
        }
        const result = yield call(sendRequest, apiParams);
        handleApiResponse(result,onCompleted,onError);
    }
    catch(error)
    {
        onError(error);
    }
}

function* createCategory({payload: {params, onCompleted, onError}})
{
    try{
        const result = yield call (sendRequest, apiConfig.category.create, params);
        handleApiResponse(result, onCompleted, onError);
    }
    catch(error)
    {
        onError(error);
    }
}

function* updateCategory({payload: {params, onCompleted, onError}})
{
    // if(!params.parentId){
    //     params.parentId = 0;
    // }
    try{
        const result = yield call (sendRequest, apiConfig.category.update, params);
        handleApiResponse(result, onCompleted, onError);
    }
    catch(error)
    {
        onError(error);
    }
}

function* deleteCategory({payload: {params, onCompleted, onError}})
{
    try{
        const apiParams = {
            ...apiConfig.category.delete,
            path: `${apiConfig.category.delete.path}/${params.id}`
        }
        const result = yield call (sendRequest, apiParams);
        handleApiResponse(result, onCompleted, onError);

        const { success, responseData } = result;
        if(!success || !responseData.result)
            yield put({ type: defineActionFailed(DELETE_CATEGORY) });
    }
    catch(error)
    {
        yield put({ type: defineActionFailed(DELETE_CATEGORY) });
        onError(error);
    }
}

const sagas = [
    takeLatest(defineActionLoading(GET_CATEGORY_LIST), getCategoryList),
    takeLatest(defineActionLoading(GET_UNIVERSITY_CATEGORY_LIST), getUniversityCategoryList),
    takeLatest(defineActionLoading(GET_JOB_CATEGORY_LIST), getJobCategoryList),

    takeLatest(actionTypes.GET_CATEGORY, getCategory),
    takeLatest(actionTypes.CREATE_CATEGORY, createCategory),
    takeLatest(actionTypes.UPDATE_CATEGORY, updateCategory),
    takeLatest(defineActionLoading(DELETE_CATEGORY), deleteCategory),
]

export default sagas;